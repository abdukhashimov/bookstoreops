from rest_framework.viewsets import ModelViewSet
from rest_framework.generics import RetrieveUpdateDestroyAPIView
from rest_framework.permissions import (
    IsAuthenticated,
    IsAdminUser,
    AllowAny,
)
from django.contrib.auth import get_user_model
from user.serializers import (
    UserListSerializer,
    UserSerializer
)
from utils.paginations import DefaultLimitOffsetPagination


class UserModelViewSet(ModelViewSet):
    queryset = get_user_model().objects.all()
    serializer_class = UserSerializer
    permission_classes = [IsAdminUser, ]
    pagination_class = DefaultLimitOffsetPagination

    def get_permissions(self):
        # in other classes
        # self.request.method == "PUT, PATCH"
        if self.action == "create":
            permission_classes = [AllowAny, ]
        else:
            permission_classes = [IsAdminUser, ]
        return [permission() for permission in permission_classes]

    def get_serializer_class(self):
        if self.action == "list":
            return UserListSerializer
        else:
            return UserSerializer


class UserControllAPIView(RetrieveUpdateDestroyAPIView):
    queryset = get_user_model().objects.all()
    serializer_class = UserSerializer
    permission_classes = [IsAuthenticated, ]

    def get_object(self):
        return self.request.user
