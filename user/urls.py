from rest_framework.routers import SimpleRouter
from django.urls import path, include
from user.views import (
    UserControllAPIView,
    UserModelViewSet,
)
router = SimpleRouter()
router.register('users', UserModelViewSet)

urlpatterns = [
    path('users/me/', UserControllAPIView.as_view(), name='user-controll'),
    path('', include(router.urls))
]
