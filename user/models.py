from django.db import models

from django.contrib.auth.models import (
    PermissionsMixin,   # is_superuser
    AbstractBaseUser,   # is_staff,
    BaseUserManager,    # provides base managing queries
)
from book.models import Book


class UserManager(BaseUserManager):

    def create_user(self, email, password=None, *args, **kwargs):
        if not email:
            raise ValueError("Users must have an email address!")
        user = self.model(email=email, *args, **kwargs)
        user.set_password(password)
        user.save(self._db)

        return user

    def create_superuser(self, email, password=None, *args, **kwargs):
        user = self.create_user(email, password, *args, **kwargs)
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)

        return user


class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(max_length=255, unique=True)
    bio = models.TextField(null=True, blank=True)
    favs = models.ManyToManyField(Book)
    created_at = models.DateTimeField(auto_now_add=True)
    # copy paste
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)

    objects = UserManager()
    USERNAME_FIELD = 'email'

    def __str__(self):
        return self.email
